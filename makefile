init:
init:
	docker pull gitlab/gitlab-runner:latest

mount:
	docker volume create gitlab-runner-config


run:
run:
	docker run -d --name gitlab-runner --restart always \
        -v /var/run/docker.sock:/var/run/docker.sock \
        -v gitlab-runner-config:/etc/gitlab-runner \
        gitlab/gitlab-runner:latest

runner:
runner: init mount run


logs:
logs:
	docker logs gitlab-runner

remove:
remove:
	docker stop gitlab-runner && docker rm gitlab-runner

register:
register:
	docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
	--executor "docker" \
	--docker-image ruby:2.6 \
	--run-untagged=true \
	--docker-privileged=true \
	--docker-volumes '/var/run/docker.sock:/var/run/docker.sock'

downloadConfig:
downloadConfig:
	docker cp gitlab-runner:/etc/gitlab-runner/config.toml gitlab-runner-helper:/etc/scripts/

updateConfig:
updateConfig:
	docker cp  gitlab-runner-helper:/etc/scripts/config.toml gitlab-runner:/etc/gitlab-runner/config.toml


showConfig:
showConfig:
	docker exec -it gitlab-runner cat /etc/gitlab-runner/config.toml


delete:
delete:
	docker volume rm gitlab-runner-config
