# How to install Gitlab Runner on your own machine using Docker

## Using the makefile

### for Windows Users: installing chocolatey and make
In order to make use of a "makefile" in windows, we need to install the Chocolatey package manager, through which we can easily install "make".<br>
To install chocolatey, open a Windows Power Shell as an Administrator and run the following command:
```Powershell
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```
Wait for the installation to finish and test it by running `choco -?`.<br>
Once you have Chocolatey installed, run:
```Powershell
choco install make
```
Now, we are able to use the "make" command in Windows.


### Creating a Runner inside a Docker Container:
To create a Runner, we use a makefile. Run:
```bash
make runner
```

### Registering a Runner:
Next we need to register our Runner with GitLab. For each runner we want to register, we have to run the following code. You can register multiple runners.
```
make register
```
For URL and Registrationtoken, use your browser to open the GitLab Project for which you want to use the Runner. Go to Settings/CI/CD-Settings and expand the "Runners"-Tab. See "setup a group runner manually" and copy the URL and Token:
![](images/url_setup.png)

Next, we need to give the runner a description (whatever suits you, e.g. "test-runner") and tags. We can ignore tags for now (they are used to allocate jobs to specific runners, which may be useful when having several runners in parallel).
Lastly, the registration process requires us to choose an executor and image. Enter:<br>
Executer:
```
docker
```
Docker Image:
``` 
ruby:2.6
```
<br>

# Congratulations! Our Runner is now set up and ready for use!
 

<br><br><br><br><br><br><br><br><br><br><br>


# *depricated*
#### this is the older version, if 'make config' worked, you can ignore this part



### Configuring the Runner for our purposes:
The Runner configuration is stored in a config.toml file inside the Runners container. 
We need to Update that config file, to enable the runner to build other docker containers.
Simply use: 
````
make config
````
This will create a containerized helper that shares a volume with the runner and runs a python script ``config_edit.py`` on the config.toml. 

You can inspect your config file with:
```make showConfig```
The config.toml file should now look something like this (Important is image, privileged and volumes):
```
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "YOUR_DECRIPTION"
  url = "https://gitlab.com/"
  token = "YOUR_TOKEN"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "docker:19.03.12"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
    shm_size = 0
```



Edit the Settings of the runner (Aftewards the runner can pic up untagged jobs)
![](images/enable_untaged.png)


You have to change the config.toml file. To download the config to your filesystem use:
```
make downloadConfig
````
Change your file so it looks like this. Important is image, privileged and volumes (works an mac os, not testet for other systems. Probably volumes could be different (please test))
```
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "build docker images"
  url = "https://gitlab.com/"
  token = "yourtoken"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "docker:19.03.12"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
    shm_size = 0
```
You have to restart your runner:
```
make updateConfig
make remove
make runner
```

## Using Docker comands
### Pull the latest Docker Image for gitlab-runner
```bash
docker pull gitlab/gitlab-runner:latest
```
### Run the Docker Image

Create the Docker volume

```bash
docker volume create gitlab-runner-config
```

Run the Runner
```bash
docker run -d --name gitlab-runner --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner-config:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest
```

Register a Runner:
```bash
docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register
```

## Some Additional Documentation
#### Check the configuration in the Docker Image.
Navigate to the root of the dockercontainer 
```
docker exec -it gitlab-runner /bin/bash
```
and run:
```
cat /etc/gitlab-runner/config.toml
```

#### Delete the docker volume
```bash
docker volume rm gitlab-runner-config
```

